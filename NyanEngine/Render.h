#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Include.h"
#include "Namespaces.h"

size_t renderDeviceSFML();
size_t renderDeviceVulkan();
void renderScene();
void loadSetTextureSprite(sf::Sprite sprite,string texture);

enum ID_Render { // ��������� ������ � �������
	ERROR_FILE,
	ERROR_LOAD
};

// Vulkan API class application


//
#pragma once

#include "Include.h"
#include "Namespaces.h"

#define u_int unsigned int

typedef struct Settings {
	string renderer = "sfml";
	u_int w = 1366;
	u_int h = 768;
	u_int frameratemax = 60;
	bool vsync = false;
	bool windowed = true;
	bool devconfig = false;
}Settings; // TODO: �������� ������ ��� ��������� �����

typedef struct GameSettings {
	string namewindow = "Nyan engine";
	size_t countscenes = 0;
	//vector<>
}GameSettings;

bool parseSettings();
bool parseGame();


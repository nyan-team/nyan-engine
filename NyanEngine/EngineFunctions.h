#pragma once

#include "Include.h"
#include "Namespaces.h"

int createBox(string texture, int x, int y, int w, int h);

struct InfoBox {
	int x;
	int y;
	int w;
	int h;
	string texture;
};
#pragma once

#include "Include.h"
#include "Namespaces.h"
#include "Render.h"

//extern std::vector<sf::Sprite*> vectorSprites;

size_t initAllocator();
size_t addAllocator(sf::Texture/* sf::Sprite* */ ptr);
sf::Texture/* sf::Sprite* */ pullerAllocator();
sf::Sprite spaceFunction(sf::Sprite ptr);
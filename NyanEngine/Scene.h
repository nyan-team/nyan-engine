#pragma once

#include "Include.h"
#include "Namespaces.h"

#include "SoundManager.h"
#include "Dialog.h"

bool InitScene();

class Scene {
public:
	string Name;
	size_t Frame;
	string Texture;
	std::vector<Dialog> dialogs; // ������ ��������
	std::vector<Sound> sounds; // ������ ������
};
